Import('env')

#print env
#print env.Dump()

if env["BOARD_F_CPU"] == "1000000L" :
    print "1MHz"
    env.Replace(FUSESCMD="avrdude $UPLOADERFLAGS -e -Ulock:w:0xFF:m -Uhfuse:w:0xDF:m -Uefuse:w:0xFF:m -Ulfuse:w:0x62:m")

elif env["BOARD_F_CPU"] == "16000000L" :
    print "16MHz"
    env.Replace(FUSESCMD="avrdude $UPLOADERFLAGS -e -Ulock:w:0xFF:m -Uhfuse:w:0xDF:m -Uefuse:w:0xFF:m -Ulfuse:w:0xF1:m")

else:
    print "8MHz"
    env.Replace(FUSESCMD="avrdude $UPLOADERFLAGS -e -Ulock:w:0xFF:m -Uhfuse:w:0xDF:m -Uefuse:w:0xFF:m -Ulfuse:w:0xE2:m")
